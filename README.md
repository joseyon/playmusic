# Proyecto: Reproductor de Musica

## Proyecto basado en el MEAN Stack

###### Proyecto desarrollado basado en el curso de Victor Robles de Udemy.

###### Pasos para su instalacion:

1.- Descargue el codigo del repositorio usando el download o haga un git clone de la url.
2.- Ingrese en la carpeta usando el comando cd proyecto_musica (ejemplo)
3.- Teclee npm install
4.- Espere que el proyecto se instale y listo
5.- Recuerde tener nodejs y angular en su version mas actualizada
6.- Fin

