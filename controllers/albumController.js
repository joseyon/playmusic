let fs= require('fs');
let path = require('path');
const mongoose = require('mongoose');
const Artist = require('../models/model_artist');
const Album = require('../models/model_album');
const Song = require('../models/model_song');
const bcrypt = require('bcryptjs');
const jwt = require('../services/jwt');
const mongoosePaginate =  require('mongoose-pagination');



//obtener un album por un Id y esta asociado a un artista
exports.getAlbum = (req, res) => {
    
    let idx = req.params.id;
    
    //obtengo los albums que estan asociados al path artista
    Album.findById(idx).populate({path:'artist'}).exec((err, albumDB)=> {
        
        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !albumDB ) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'El album solicitado no existe'
            });
        }

        return res.json({
            album: albumDB,
            ok: true,
            msg: 'Album obtenido con exito'     
        });
    });
}


//Guardar un Album
exports.saveAlbum = async (req, res) => {

    const {title, description, year, artist, image} = req.body;

    //llamamos el modelo de base de datos con el objeto que proviene de la peticion
    const album = new Album({
        title, description, year, artist, image
    });

    //return console.log(album);

    await album.save( (err, albumDB) => {

        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !albumDB ) {
            return res.status(400).json({
                ok:false,
                error: err,
                msg: 'Los datos del Album no pudieron ser almacenados. Intente más tarde'
            });
        }

        return res.json({
            artista: albumDB,
            ok: true,
            msg: 'Los datos del album fueron almacenados exitosamente'            
        });
    });
}


//Obtener todos los album y paginar
exports.getFullAlbums = (req, res) => {

    //necesito todos los albums de un artista
    let artistId = req.params.artist;

    var page= req.params.page;


    if(req.params.page){
        var page= req.params.page;
    }
    else {
        var page = 1;
    }

    let itemsPerPage = 2;

    if(!artistId){
        //Sacamos todos los albums de la base de datos porque no hay id del artista
        Album.find({}).sort('year').exec((err, albumDB) =>{
            if( err ) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error al realizar la petición'
                });
            }
    
            if( !albumDB ) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'No existen albums almacenados asociados al artista'
                });
            }
    
            Album.countDocuments().exec(function(err, count) {
                return res.json({
                    ok: true,
                    album: albumDB,
                    total_items: count,            
                    msg: 'Lista de Albums Obtenida Satisfactoriamente asociados al artista'    
                });     
             });        
        });
    }
    else {
        //sacar los albums asociado a la busqueda de un artista seleccionado
        Album.find({artist: artistId}).sort('year').populate({path:'artist'}).paginate(page, itemsPerPage).exec((err, albumDB) =>{
            if( err ) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error al realizar la petición'
                });
            }
    
            if( !albumDB ) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'No existen albums almacenados asociados al artista'
                });
            }
    
            Album.countDocuments().exec(function(err, count) {
                return res.json({
                    ok: true,
                    album: albumDB,
                    total_items: count,            
                    msg: 'Lista de Albums Obtenida Satisfactoriamente asociados al artista'    
                });     
             });        
        });
    }
}

//Actualizar un Album
exports.updateAlbum = (req, res) => {

    let idx = req.params.id;

    let data = req.body;

    Album.findByIdAndUpdate(idx, data, {new: true, runValidators: true}, (err, albumDB) => {
        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!albumDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! El Album no pudo actualizarse'
            });
        }
        
        return res.status(200).json({
            ok: true,
            albumDB,
            msg: 'El Album se ha actualizado exitosamente'
        });
    });
}

exports.deleteAlbum = (req, res) => {
    
    let albumId = req.params.id;

    Album.findByIdAndRemove(albumId, (err, albumDB)=>{       
        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!albumDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! El Album no pudo actualizarse'
            });
        }

        Song.find({album:albumDB._id}).remove((err, songDB) => {
            if (err) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error en la peticion. Intente Nuevamente'
                });
            }
    
            if (!songDB) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'Error!! Las Canción no pudo eliminarse'
                });
            }

            return res.status(200).json({
                ok: true,
                albumDB,
                msg: 'El album se ha eliminado exitosamente'
            });
        });
    });    
}

exports.uploadImageAlbum = (req, res) => {

    let idx = req.params.id;

    let file_name = 'Imagen No Subida';

    if(req.files){

        let file_path = req.files.image.path;

        let file_path_split = file_path.split('\\');

        let file_name = file_path_split[2];

        let ext_split = file_name.split('\.')

        let file_ext = ext_split[1];      
        
        if(file_ext === 'png' || file_ext === 'jpg' || file_ext === 'gif') {
            Album.findByIdAndUpdate(idx, {image: file_name}, {new: true, runValidators: true}, (err, usuarioDb) => {

                if (err) {
                    return res.status(500).json({
                        ok:false,
                        error: err,
                        msg: 'Error en la peticion. Intente Nuevamente'
                    });
                }
        
                if (!usuarioDb) {
                    return res.status(404).json({
                        ok:false,
                        error: err,
                        msg: 'Error!! No se pudo actualizar la imagen del album'
                    });
                }
                
                res.status(200).json({
                    ok: true,
                    usuarioDb,
                    msg: 'La imagen del album se ha actualizado exitosamente'
                });
            }); 
        }

        else {
            res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La extensión del fichero no es valida'
            });
        }
    }

    else {
        res.status(404).json({
            ok:false,
            error: err,
            msg: 'Error!! No has subido ninguna imagen'
        });
    }

}


exports.getImageFile = (req, res) => {

    let imageFile = req.params.imageFile;

    let fileImage = './upload/albums/' + imageFile ;

    fs.exists(fileImage, (image) =>{

        if(!image){
            return res.status(500).json({
                ok: false,
                msg: 'No existe la imagen solicitada'
            });
        }

        return res.sendFile(path.resolve(fileImage));
    });


}