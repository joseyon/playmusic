let fs= require('fs');
let path = require('path');
const mongoose = require('mongoose');
const Artist = require('../models/model_artist');
const Album = require('../models/model_album');
const Song = require('../models/model_song');
const bcrypt = require('bcryptjs');
const jwt = require('../services/jwt');
const mongoosePaginate =  require('mongoose-pagination');


//Obtener un Artista dado un Id
exports.getArtist = (req, res) => {
    
    let idx = req.params.id;

    Artist.findById(idx, (err, artistDB)=> {
        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !artistDB ) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'El artista solicitado no existe'
            });
        }

        return res.json({
            artista: artistDB,
            ok: true            
        });
    });

}

//Guardar un Artista
exports.saveArtist = async (req, res) => {

    const {name, description, image} = req.body;

    //llamamos el modelo de base de datos con el objeto que proviene de la peticion
    const artist = new Artist({
        name, description, image
    });

    await artist.save( (err, artistDB) => {

        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !artistDB ) {
            return res.status(400).json({
                ok:false,
                error: err,
                msg: 'Los datos del Artista no pudieron ser almacenados. Intente más tarde'
            });
        }

        //Para no mostrar el password en la respuesta
        //usuarioDB.password = null;

        return res.json({
            artista: artistDB,
            ok: true,
            msg: 'Los datos del artista fueron almacenados exitosamente'            
        });
    });

}


exports.getFullArtists = (req, res) => {

    let page= req.params.page;

    if(req.params.page){
        let page= req.params.page;
    }
    else {
        let page = 1;
    }

    let itemsPerPage = 3;

    //Artist.find().sort('name').paginate(page, itemsPerPage),((err, artistDB) =>{
    Artist.find({}).sort('name').paginate(page, itemsPerPage).exec((err, artistDB) =>{
        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !artistDB ) {
            return res.status(400).json({
                ok:false,
                error: err,
                msg: 'No existen artistas almacenados'
            });
        }

        Artist.countDocuments().exec(function(err, count) {
            return res.json({
                ok: true,
                artista: artistDB,
                total_items: count,            
                msg: 'Lista de Artistas Obtenida Satisfactoriamente'    
            });     
         });        
    });
}

exports.updateArtist = (req, res) => {

    let idx = req.params.id;

    let data = req.body;

    Artist.findByIdAndUpdate(idx, data, {new: true, runValidators: true}, (err, artistDB) => {

        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!artistDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! El Artista no pudo actualizarse'
            });
        }
        
        return res.status(200).json({
            ok: true,
            artistDB,
            msg: 'El artista se ha actualizado exitosamente'
        });
    }); 
}

exports.deleteArtist = (req, res) => {

    let idx = req.params.id;

    Artist.findByIdAndRemove(idx, (err, artistDB) =>{
        
        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!artistDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! El Artista no pudo eliminarse'
            });
        }

        Album.find({artist:artistDB._id}).remove((err, albumDB) => {
            if (err) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error en la peticion. Intente Nuevamente'
                });
            }
    
            if (!albumDB) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'Error!! El Album no pudo eliminarse'
                });
            }

            Song.find({album:albumDB._id}).remove((err, songDB) => {
                if (err) {
                    return res.status(500).json({
                        ok:false,
                        error: err,
                        msg: 'Error en la peticion. Intente Nuevamente'
                    });
                }
        
                if (!songDB) {
                    return res.status(404).json({
                        ok:false,
                        error: err,
                        msg: 'Error!! Las Canción no pudo eliminarse'
                    });
                }
    
                return res.status(200).json({
                    ok: true,
                    artistDB,
                    msg: 'El artista se ha eliminado exitosamente'
                });
            });
        });
    });

}


exports.uploadImageArtist = (req, res) => {

    let idx = req.params.id;

    let file_name = 'Imagen No Subida';

    if(req.files){

        let file_path = req.files.image.path;

        let file_path_split = file_path.split('\\');

        let file_name = file_path_split[2];

        let ext_split = file_name.split('\.')

        let file_ext = ext_split[1];      
        
        if(file_ext === 'png' || file_ext === 'jpg' || file_ext === 'gif') {
            Artist.findByIdAndUpdate(idx, {image: file_name}, {new: true, runValidators: true}, (err, usuarioDb) => {

                if (err) {
                    return res.status(500).json({
                        ok:false,
                        error: err,
                        msg: 'Error en la peticion. Intente Nuevamente'
                    });
                }
        
                if (!usuarioDb) {
                    return res.status(404).json({
                        ok:false,
                        error: err,
                        msg: 'Error!! No se pudo actualizar la imagen del artista'
                    });
                }
                
                res.status(200).json({
                    ok: true,
                    usuarioDb,
                    msg: 'La imagen del artista se ha actualizado exitosamente'
                });
            }); 
        }

        else {
            res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La extensión del fichero no es valida'
            });
        }
    }

    else {
        res.status(404).json({
            ok:false,
            error: err,
            msg: 'Error!! No has subido ninguna imagen'
        });
    }

}


exports.getImageFile = (req, res) => {

    let imageFile = req.params.imageFile;

    let fileImage = './upload/artists/' + imageFile ;

    fs.exists(fileImage, (image) =>{

        if(!image){
            return res.status(500).json({
                ok: false,
                msg: 'No existe la imagen solicitada'
            });
        }

        return res.sendFile(path.resolve(fileImage));
    });


}