let fs= require('fs');
let path = require('path');
const mongoose = require('mongoose');
const Artist = require('../models/model_artist');
const Album = require('../models/model_album');
const Song = require('../models/model_song');
const bcrypt = require('bcryptjs');
const jwt = require('../services/jwt');
const mongoosePaginate =  require('mongoose-pagination');


exports.saveSong = async (req, res) => {
    
    const {number, name, duration, file, album} = req.body;

    //llamamos el modelo de base de datos con el objeto que proviene de la peticion
    const song = new Song({
        number, name, duration, file, album
    });

    await song.save( (err, songDB) => {

        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !songDB ) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Los datos de la Canción no pudieron ser almacenados. Intente más tarde'
            });
        }

        return res.json({
            cancion: songDB,
            ok: true,
            msg: 'Los datos de la Canción fueron almacenados exitosamente'            
        });
    });

}

exports.getSong = (req, res) => {

    let idx = req.params.id;

    Song.findById(idx).populate({path: 'album'}).exec((err, songDB)=>{

        if( err ) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error al realizar la petición'
            });
        }

        if( !songDB ) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'La canción solicitada no existe'
            });
        }

        return res.json({
            cancion: songDB,
            ok: true            
        });
    }); 
}

exports.getFullSongs = (req, res) => {

    //sacamos las canciones asociadas a un album
    let albumId = req.params.id;

    if(!albumId){
        Song.find({}).sort('number').populate({path:'album', populate:{path:'artist', model: 'ArtistaModelo'}}).exec((err, songDB)=>{
            if( err ) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error al realizar la petición'
                });
            }
    
            if( !songDB ) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'La canción solicitada no existe'
                });
            }
    
            return res.json({
                cancion: songDB,
                ok: true            
            });
        });
    }
    else {
        Song.find({album: albumId}).sort('number').populate({path:'album', populate:{path:'artist', model: 'ArtistaModelo'}}).exec((err,songDB)=>{
            if( err ) {
                return res.status(500).json({
                    ok:false,
                    error: err,
                    msg: 'Error al realizar la petición'
                });
            }
    
            if( !songDB ) {
                return res.status(404).json({
                    ok:false,
                    error: err,
                    msg: 'La canción solicitada no existe'
                });
            }
    
            return res.json({
                cancion: songDB,
                ok: true            
            });
        });
    }
}

exports.updateSong = (req, res) => {
    
    let idx = req.params.id;

    let data = req.body;

    Song.findByIdAndUpdate(idx, data, {new: true, runValidators: true}, (err, songDB) => {

        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!songDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La canción no pudo actualizarse'
            });
        }
        
        return res.status(200).json({
            ok: true,
            songDB,
            msg: 'La Canción se ha actualizado exitosamente'
        });
    }); 
}

exports.deleteSong = (req, res) =>{
    let idx = req.params.id;

    Song.findByIdAndRemove(idx, (err, albumDB) =>{
        
        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!albumDB) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La Canción no pudo eliminarse'
            });
        }

        return res.status(200).json({
            ok: true,
            albumDB,
            msg: 'La Cancion se ha eliminado exitosamente'
        });
    });
}


exports.uploadSong = (req, res) => {

    let idx = req.params.id;

    let file_name = 'Imagen No Subida';

    if(req.files){

        //no va files.images sino files.file
        let file_path = req.files.file.path;

        let file_path_split = file_path.split('\\');

        let file_name = file_path_split[2];

        let ext_split = file_name.split('\.')

        let file_ext = ext_split[1];      
        
        if(file_ext === 'mp3') {
            Song.findByIdAndUpdate(idx, {file: file_name}, {new: true, runValidators: true}, (err, usuarioDb) => {

                if (err) {
                    return res.status(500).json({
                        ok:false,
                        error: err,
                        msg: 'Error en la peticion. Intente Nuevamente'
                    });
                }
        
                if (!usuarioDb) {
                    return res.status(404).json({
                        ok:false,
                        error: err,
                        msg: 'Error!! No se pudo actualizar la canción'
                    });
                }
                
                res.status(200).json({
                    ok: true,
                    usuarioDb,
                    msg: 'La canción del artista se ha cargado exitosamente'
                });
            }); 
        }

        else {
            res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La extensión del fichero no es valida'
            });
        }
    }

    else {
        res.status(404).json({
            ok:false,
            error: err,
            msg: 'Error!! No has subido ningun archivo'
        });
    }

}


exports.getSongFile = (req, res) => {

    let songFile = req.params.songFile;

    let fileSong = './upload/songs/' + songFile ;

    fs.exists(fileSong, (song) =>{

        if(!song){
            return res.status(500).json({
                ok: false,
                msg: 'No existe el archivo de audio solicitado'
            });
        }

        return res.sendFile(path.resolve(fileSong));
    });
}