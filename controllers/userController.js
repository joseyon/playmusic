let fs= require('fs');
let path = require('path');
const mongoose = require('mongoose');
const User = require('../models/model_user');
const bcrypt = require('bcryptjs');
const jwt = require('../services/jwt');


//Metodo de Almacenar un Usuario en el Sistema de Musica
exports.saveUser = async (req, res) => {
    
    //let data = req.body;

    const {name, surname, email, password, role, image} = req.body;

    //llamamos el modelo de base de datos con el objeto que proviene de la peticion
    const user = new User({
        name, surname, email, password: bcrypt.hashSync(password, 10), role, image
    });

    await user.save( (err, usuarioDB) => {

        if( err ) {
            return res.status(400).json({
                ok:false,
                error: err,
                msg: 'Los datos no pudieron ser almacenados. Intente más tarde'
            });
        }

        //Para no mostrar el password en la respuesta
        //usuarioDB.password = null;

        res.json({
            persona: usuarioDB,
            ok: true,
            msg: 'Los datos del usuario fueron almacenados exitosamente'            
        });
    });
}


//Metodo para loguear un Usuario en el Sistema de Musica
exports.loginUser = (req, res) => {

    const {email, password} = req.body;

    User.findOne({ email: email.toLowerCase()},  (err, user) => {

        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });       
        }

        if(!user){
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'El Usuario no existe o es incorrecto'
            });
        }

        let new_pass = bcrypt.compareSync(password, user.password) ;

        if(!new_pass){
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'La contraseña es incorrecta'
            });
        }

        //Genero el Token porque ha pasado todas las validaciones
        let token = jwt.createToken(user);
        
        return res.status(200).json({
            ok: true,
            user,
            token
        });
    });

}

//Metodo para actualizar  un Usuario en el Sistema de Musica
exports.updateUser= (req, res) => {

    let idx = req.params.id;

    let data = req.body;

    User.findByIdAndUpdate(idx, data, {new: true, runValidators: true}, (err, usuarioDb) => {

        if (err) {
            return res.status(500).json({
                ok:false,
                error: err,
                msg: 'Error en la peticion. Intente Nuevamente'
            });
        }

        if (!usuarioDb) {
            return res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! El usuario no pudo actualizarse'
            });
        }
        
        return res.status(200).json({
            ok: true,
            usuarioDb,
            msg: 'El usuario se ha actualizado exitosamente'
        });
    }); 
}

exports.uploadImage = (req, res) => {
    let idx = req.params.id;

    let file_name = 'Imagen No Subida';

    if(req.files){

        let file_path = req.files.image.path;

        let file_path_split = file_path.split('\\');

        let file_name = file_path_split[2];

        let ext_split = file_name.split('\.')

        let file_ext = ext_split[1];      
        
        if(file_ext === 'png' || file_ext === 'jpg' || file_ext === 'gif') {
            User.findByIdAndUpdate(idx, {image: file_name}, {new: true, runValidators: true}, (err, usuarioDb) => {

                if (err) {
                    return res.status(500).json({
                        ok:false,
                        error: err,
                        msg: 'Error en la peticion. Intente Nuevamente'
                    });
                }
        
                if (!usuarioDb) {
                    return res.status(404).json({
                        ok:false,
                        error: err,
                        msg: 'Error!! No se pudo actualizar la imagen del usuario'
                    });
                }
                
                res.status(200).json({
                    ok: true,
                    usuarioDb,
                    msg: 'La imagen del usuario se ha actualizado exitosamente'
                });
            }); 
        }

        else {
            res.status(404).json({
                ok:false,
                error: err,
                msg: 'Error!! La extensión del fichero no es valida'
            });
        }
    }

    else {
        res.status(404).json({
            ok:false,
            error: err,
            msg: 'Error!! No has subido ninguna imagen'
        });
    }
}

exports.getImageFile = (req, res) => {

    let imageFile = req.params.imageFile;

    let fileImage = './upload/users/' + imageFile ;

    fs.exists(fileImage, (image) =>{

        if(!image){
            return res.status(500).json({
                ok: false,
                msg: 'No existe la imagen solicitada'
            });
        }

        return res.sendFile(path.resolve(fileImage));
    });


}