'use strict'

const express = require('express');
const app = express();
const moongoose = require('mongoose');
const bodyParser = require('body-parser');
const index_routes = require ('./routes/index');
const PORT= 3000;


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());

//Configuracion Global de rutas (index de la carpeta routes)
app.use(index_routes);


//Conexion al Servidor Web Local
app.listen(PORT, function() {
    console.log('server running on http://localhost:' + PORT);
});

//conexion con MongoDB
moongoose.set('useNewUrlParser', true);
moongoose.set('useUnifiedTopology', true);
moongoose.set('useCreateIndex', true);

moongoose.connect('mongodb://localhost:27017/my_db_music', (err, resp) => {
    if(err){
        throw err;
    }
    
    console.log('conexion a base de datos Music en Mongo Exitosa');

});