const jwt = require('jwt-simple');

const moment = require ('moment');

let secret = 'clave_secreta_curso';

exports.ensureAuth = (req, res, next) => {
    if(!req.headers.authorization){
        return res.status(403).json({
            ok:false,
            msg: 'La peticion no tiene cabecera de autenticacion'
        });
    }

    let token = req.headers.authorization.replace(/['"]+/g, '');

    

    try{
        
        var payload = jwt.decode(token, secret);

        if(payload.exp <= moment.unix()){
            return res.status(401).json({
                ok:false,
                msg: 'El token ha expirado'
            });
        }
    }

    catch{
        return res.status(404).json({
            ok:false,
            msg: 'El token no es valido'
        });
    }

    req.user = payload;

    next();

}
    