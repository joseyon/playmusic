'use strict'

const mongoose = require ('mongoose');
const  uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let albumSchema = new Schema({
    title:{
        type: String,
        required: [true, 'El campo Titulo es Obligatorio']
    },

    description: {
        type: String,
        required: [true, 'El campo Descrpción es Obligatorio']
    },

    year: {
        type: String,
        required: [true, 'El campo Año es Obligatorio']
    },

    artist:{
        type: Schema.ObjectId,
        ref: 'ArtistaModelo'
    },

    image: {
        type: String,
        default: 'null'
    }

});


// Apply the uniqueValidator plugin to userSchema.
albumSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico. Este valor ya esta registrado' });


module.exports = mongoose.model('AlbumModelo', albumSchema);