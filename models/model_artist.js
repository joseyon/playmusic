'use strict'

const mongoose = require ('mongoose');
const  uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let artistSchema = new Schema({
    name:{
        type: String,
        required: [true, 'El campo Nombre es Obligatorio']
    },

    description: {
        type: String,
        required: [true, 'El campo Descripción es Obligatorio']
    },

    image: {
        type: String,
        default: 'null'
    }

});


// Apply the uniqueValidator plugin to userSchema.
artistSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico. Este valor ya esta registrado' });


module.exports = mongoose.model('ArtistaModelo', artistSchema);