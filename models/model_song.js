'use strict'

const mongoose = require ('mongoose');
const  uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let songSchema = new Schema({
    number:{
        type: Number,
        required: [true, 'El campo numero es Obligatorio']
    },

    name: {
        type: String,
        required: [true, 'El campo Nombre es Obligatorio']
    },

    duration: {
        type: String,
        required: [true, 'El campo duracion es obligatorio']
    },

    file: {
        type: String,
        default: 'null'
    },

    album:{
        type: Schema.ObjectId,
        ref: 'AlbumModelo'
    }

});


// Apply the uniqueValidator plugin to userSchema.
songSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico. Este valor ya esta registrado' });


module.exports = mongoose.model('SongModelo', songSchema);