'use strict'

const mongoose = require ('mongoose');
const  uniqueValidator = require('mongoose-unique-validator');

let roles_validos = {
    values : ['ADMIN_ROLE', 'USER_ROLE'],
    msg: '{VALUE} no es un role validos'
}

let Schema = mongoose.Schema;

let userSchema = new Schema({
    name:{
        type: String,
        required: [true, 'El campo Nombre es Obligatorio']
    },

    surname: {
        type: String,
        required: [true, 'El campo Apellidos es Obligatorio']
    },

    email: {
        type: String,
        required: [true, 'El campo Email es Obligatorio'],
        unique: true
    },

    password:{
        type: String,
        required: [true, 'El campo password es obligatorio']
    },

    role: {
        type: String,
        default: 'USER_ROLE',
        enum: roles_validos
    },

    image: {
        type: String
    }

});


// Apply the uniqueValidator plugin to userSchema.
userSchema.plugin(uniqueValidator, {message: '{PATH} debe ser unico. Este valor ya esta registrado' });


module.exports = mongoose.model('UsuarioModelo', userSchema);