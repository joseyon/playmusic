const express = require('express');
const router = express.Router();
const albumController = require('../controllers/albumController');
const md_auth = require('../middlewares/authentication');

const multiparty = require('connect-multiparty')
md_upload = multiparty({
    uploadDir: './upload/albums'
    //pendiente con esta ruta Usa una sola barra para encontrar la carpeta
});


router.get('/get-album/:id', md_auth.ensureAuth, albumController.getAlbum);

router.post('/album', md_auth.ensureAuth, albumController.saveAlbum);

router.get('/getfull-albums/:artist?/:page?', md_auth.ensureAuth, albumController.getFullAlbums);

router.put('/update-album/:id', md_auth.ensureAuth, albumController.updateAlbum);

router.delete('/delete-album/:id', md_auth.ensureAuth, albumController.deleteAlbum);

router.post('/upload-image-album/:id', [md_auth.ensureAuth, md_upload], albumController.uploadImageAlbum);

router.get('/get-image-album/:imageFile', albumController.getImageFile);




module.exports = router;