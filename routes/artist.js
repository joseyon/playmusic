const express = require('express');
const router = express.Router();
const artistController = require('../controllers/artistController');
const md_auth = require('../middlewares/authentication');

const multiparty = require('connect-multiparty')
md_upload = multiparty({
    uploadDir: './upload/artists'
    //pendiente con esta ruta Usa una sola barra para encontrar la carpeta
});



router.get('/get-artist/:id', md_auth.ensureAuth, artistController.getArtist);

router.post('/artist', md_auth.ensureAuth, artistController.saveArtist);

router.get('/getfull-artist/:page?', md_auth.ensureAuth, artistController.getFullArtists);

router.put('/update-artist/:id', md_auth.ensureAuth, artistController.updateArtist);

router.delete('/delete-artist/:id', md_auth.ensureAuth, artistController.deleteArtist);

router.post('/upload-image-artist/:id', [md_auth.ensureAuth, md_upload], artistController.uploadImageArtist);

router.get('/get-image-artist/:imageFile', artistController.getImageFile);





module.exports = router;