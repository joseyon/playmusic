const express = require('express');
const app = express();

//configurar cabeceras http
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Request-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTION, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTION, PUT, DELETE');

    next();

});

//Listado de rutas del Proyecto

//invocar
const user_routes = require ('./user');
const song_routes = require ('./song');
const artist_routes = require ('./artist');
const album_routes = require ('./album');

//implementar
app.use(user_routes);
app.use(song_routes);
app.use(artist_routes);
app.use(album_routes);


module.exports = app;