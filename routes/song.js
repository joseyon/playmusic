const express = require('express');
const router = express.Router();
const songController = require('../controllers/songController');
const md_auth = require('../middlewares/authentication');

const multiparty = require('connect-multiparty')
md_upload = multiparty({
    uploadDir: './upload/songs'
    //pendiente con esta ruta Usa una sola barra para encontrar la carpeta
});



router.post('/song', md_auth.ensureAuth, songController.saveSong);

router.get('/get-song/:id', md_auth.ensureAuth, songController.getSong);

router.get('/getfull-songs/:id?', md_auth.ensureAuth, songController.getFullSongs);

router.put('/update-song/:id', md_auth.ensureAuth, songController.updateSong);

router.delete('/delete-song/:id', md_auth.ensureAuth, songController.deleteSong);

router.post('/upload-song-album/:id', [md_auth.ensureAuth, md_upload], songController.uploadSong);

router.get('/get-song-album/:songFile', songController.getSongFile);

module.exports = router;