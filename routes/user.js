const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const md_auth = require('../middlewares/authentication');

const multiparty = require('connect-multiparty')
md_upload = multiparty({
    uploadDir: './upload/users'
    //pendiente con esta ruta Usa una sola barra para encontrar la carpeta
});

router.post('/register', userController.saveUser);

router.post('/login', userController.loginUser);

router.put('/update-user/:id', md_auth.ensureAuth , userController.updateUser);

router.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], userController.uploadImage);

router.get('/get-image-user/:imageFile', userController.getImageFile);


module.exports = router;